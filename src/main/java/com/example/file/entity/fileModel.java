package com.example.file.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="manager")
@Data
public class fileModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	
	private String contentType;
	
	@Column(name="fileName")
	private String fileName;
	private byte[] data;


	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id=id;
		
	}
	
	
		public String getFileName() {
			
		return fileName;
	}
	
	@Column(name="data")
	public byte[] getdata() {
		 return data;
			
		}
	
	public void setdata(byte[] bytes) {
		 this.data=data;
			
		}


	public fileModel() {
		super();
	}


	public fileModel(long id, String contentType, String fileName, byte[] data) {
		this.id = id;
		this.contentType = contentType;
		this.fileName = fileName;
		this.data = data;
	}

	public String setfileName(String originalFilename) {
		return originalFilename;
		
	}

	public String setcontentType(String contentType2) {
		
		return contentType2;
	}


	
	
	
	
	
}
