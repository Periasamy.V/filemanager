package com.example.file.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.file.entity.fileModel;
import com.example.file.repository.FileRepository;

@Service

public class FileService {

	@Autowired
	private FileRepository fileRepository;
	
	
	public fileModel saveFile(MultipartFile file) throws IOException {
		fileModel fileModel=new fileModel();
		fileModel.setcontentType(file.getContentType());
		fileModel.setfileName(file.getOriginalFilename());
		fileModel.setdata(file.getBytes());
		return fileRepository.save(fileModel);
		
		
	}
	
	
	
	
}
